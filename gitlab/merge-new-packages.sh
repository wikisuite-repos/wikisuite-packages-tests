#!/bin/bash

RESULTS=$1

AUX="${RESULTS}.aux"
PAGES="${RESULTS}.pages"

# https://gitlab.com/wikisuite/wikisuite-packages/-/jobs/artifacts/main/browse?job=pages
PAGES_DOWNLOAD_URL="https://gitlab.com/api/v4/projects/32909709/jobs/artifacts/main/download?job=pages"

# Download and extract pages
mkdir -p ${PAGES}
curl -L "${PAGES_DOWNLOAD_URL}" --output pages.zip
if [ -f pages.zip ] ; then
  unzip pages.zip -d ${PAGES}
fi

# Extract old results
mkdir -p ${AUX}
if [ -f "${PAGES}/public/result.zip" ] ; then
  unzip "${PAGES}/public/result.zip" -d "${AUX}"
fi

# Make sure all new folders are available in the old results
find "${RESULTS}" -type d | while read -r D ; do
  if [ ! -d "${AUX}/result/${D/${RESULTS}/}" ] ; then
    mkdir -p "${AUX}/result/${D/${RESULTS}/}"
  fi
done

# check if we take a new version of keep the old one
# Params:
# 1 - version file to compare
# 2 - prefix of files/packages to move
# 3 - remove/keep packages if same version
check_version_and_move () {
  local oldVersion=$(cat "${AUX}/result/$1")
  local newVersion=$(cat "${RESULTS}/$1")
  local remove="${3:-remove}"

  if [ "${oldVersion}" == "${newVersion}" ] ; then
    if [ "${remove}" == "remove" ] ; then
      find "${RESULTS}" -iname "$2*" -exec rm {} \;
    fi
  else
    find "${RESULTS}" -iname "$2*" | while read -r F ; do
      mv "${F}" "${AUX}/result/${F/${RESULTS}/}"
    done
  fi
}

# if main repo changed, move all packages (don't deleted if didn't change)
check_version_and_move gitlab_wikisuite_wikisuite-packages_version wikisuite- keep

# Package: wikisuite-tikimanager
# Dependency: https://gitlab.com/tikiwiki/tiki-manager/
check_version_and_move gitlab_tikiwiki_tiki-manager_version wikisuite-tikimanager_

# Package: wikisuite-virtualmin-tikimanager
# Dependency: https://gitlab.com/wikisuite/virtualmin-tikimanager
check_version_and_move gitlab_wikisuite_virtualmin-tikimanager_version wikisuite-virtualmin-tikimanager_

# Package: wikisuite-virtualmin-tikimanager-installer
# Dependency: https://gitlab.com/wikisuite/tiki-manager-for-virtualmin
check_version_and_move gitlab_wikisuite_tiki-manager-for-virtualmin_version wikisuite-virtualmin-tikimanager-installer_

# Package: wikisuite-virtualmin-syncthing
# Dependency: https://gitlab.com/wikisuite/virtualmin-syncthing
check_version_and_move gitlab_wikisuite_virtualmin-syncthing_version wikisuite-virtualmin-syncthing_

# Package: wikisuite-virtualmin-syncthing-installer
# Dependency: https://gitlab.com/wikisuite/syncthing-for-virtualmin
check_version_and_move gitlab_wikisuite_syncthing-for-virtualmin_version wikisuite-virtualmin-syncthing-installer_

# The rest of the packages
check_version_and_move gitlab_wikisuite_wikisuite-packages_version wikisuite-

# Delete old version files
rm "${AUX}/result/*_version"

# move the rest of the files across
find "${RESULTS}" -type f | while read -r F ; do
  mv "${F}" "${AUX}/result/${F/${RESULTS}/}"
done

# Swap folders
mv "${RESULTS}" "${RESULTS}.old"
mv "${AUX}/result" "${RESULTS}"

# Todo - limit the number of versions that we keep over time

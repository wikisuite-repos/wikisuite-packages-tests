#!/bin/bash

TARGET_DIR=$1

if [ ! -d ${TARGET_DIR} ] ; then
  echo "Error: Target folder '${TARGET_DIR}' does not exists"
  exit 1
fi

download_version () {
  local url="https://gitlab.com/api/v4/projects/$1/repository/commits/master"
  local version_file="${TARGET_DIR}/$2"
  local json=$(curl -s -S -L ${url})
  local result=$?
  if [ $result -ne 0 ] ; then
      echo "Error: There was an issue downloading the version from ${url}"
      exit 2
  fi
  echo $json | sed 's/.*"id":"\([a-z0-9]*\)".*/\1/' > ${version_file}
}

# Package: wikisuite-content-processing, wikisuite-elasticsearch, wikisuite-external-repositories, wikisuite-manticore, wikisuite-php
#     wikisuite-syncthing, wikisuite-sysadmin, wikisuite-virtualmin-all, wikisuite-virtualmin-base, wikisuite-virtualmin-elasticsearch
# Dependency: https://gitlab.com/wikisuite/wikisuite-packages
if [ -z "${CI_COMMIT_SHA}" ] ; then
  download_version 9558938 gitlab_wikisuite_wikisuite-packages_version
else
  # if running as part of CI, take the version reported by CI
  echo ${CI_COMMIT_SHA} > "${TARGET_DIR}/gitlab_wikisuite_wikisuite-packages_version"
fi

# Package: wikisuite-tikimanager
# Dependency: https://gitlab.com/tikiwiki/tiki-manager/
download_version 9558938 gitlab_tikiwiki_tiki-manager_version

# Package: wikisuite-virtualmin-tikimanager
# Dependency: https://gitlab.com/wikisuite/virtualmin-tikimanager
download_version 28063542 gitlab_wikisuite_virtualmin-tikimanager_version

# Package: wikisuite-virtualmin-tikimanager-installer
# Dependency: https://gitlab.com/wikisuite/tiki-manager-for-virtualmin
download_version 23451252 gitlab_wikisuite_tiki-manager-for-virtualmin_version

# Package: wikisuite-virtualmin-syncthing
# Dependency: https://gitlab.com/wikisuite/virtualmin-syncthing
download_version 45215768 gitlab_wikisuite_virtualmin-syncthing_version

# Package: wikisuite-virtualmin-syncthing-installer
# Dependency: https://gitlab.com/wikisuite/syncthing-for-virtualmin
download_version 23458479 gitlab_wikisuite_syncthing-for-virtualmin_version
